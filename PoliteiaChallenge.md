### Proof of Jerky and The Moon Landing

What do these things have in common?
An immense amount of data that corroborates with itself.
We don't need to trust scientists because we can run experiments that prove it's correctness.
If their is a political motivation to falsify science, it's most likely a countervailing force who is motivated to show this falsity.
Science proves itself.
One fact that runs counter to theory is enough to throw out the theory.
Science can be lead astray by bad data.
Given valid data science will come to the same conclusions every time.


### The Challenge
is to come up with an alternative use of the Politeia codebase to solve problems that are not a proposal system.

### Vague Outline
If you haven't already I'd check out the [#D draft whitepaper](https://hashd.in/hashd-in-draft0/) I think it has useful ideas that can be applied here.

The goal is provenance on the blockchain.
The value isn't in knowing that something happened when.
The value is in knowing who said what happened when.
In this case the who are sensors which will track the production of jerky.
The value of the who is knowing whether the data they provide is valid.
Since there is actually no viable way of doing this in a cryptographically trusted way we must fall back on what got us to here, trust in one another.
More specifically trust over time, reputation, it's the currency of human and economic organization.

Starting a reputation system will always have the nothing at stake problem but this is a PoC, with enough history and users a user's reputation will be valuable enough to not want to lose.

#### Putting Something at Stake and Increasing Bandwidth Efficiency
One proven way to create a system you can trust without trusting any participants while simultaneously being resistant to censorship is Proof of Work(PoW).
Contradicting what I mentioned before there is one exception to not being able cryptographically trust a sensor reading value, this is PoW.
It's super limited and only measures itself but the amount of energy spent on a hash can be calculated to reasonable accuracy.
Adding proof of work to identities will do two things.
One, adds cost that is a complete waste since the PoW done is tied only to that identity and cannot be reused.
This can help with the nothing at stake problem since the only value of the identity is that it can be trusted, betraying that trust destroys the value of that identity, taking with it any PoW done on it.
Second it rate limits the creation of identities.
While there wouldn't be a hard threshold for the amount of PoW needed to be considered valid it can act as a filtering mechanism which clients can use to reduce the amount of bandwidth needed, simply denying communication from identities that were cheaply created.
Reducing the number of identities will also make the search space much smaller when trying to tease out sybil nodes or networks.


### Politeia Challenge Proof of concept
Identities could be either client & server or just client.
Creating identities would be the first order of business.
To do this we'd create a client server pair for each identity.
The client would then create an identity, and submit proposal containing identity related data to itself(server).
Once the initial identity block is created we have enough entropy to apply proof of work of work to the first proposal(identity block), using a modified gominer client PoW is done on the merkleroot, once enough PoW is spent it adds the result to the proposal, then submits it to the other identities' servers.(this could be foregone since we already trust each other and just submit the first proposal to each other)

Now we have three identities who collectively have spent some amount of money($20-$2000) on their identities.
We can now make attestations about each other, ie  I trust X in a business context or Y is knowledgeable about blockchains.
We can then create identities for the sensors which will be recording the data.
The sensors will probably be all raspberry pi 3's each pi creating an identity with all of the other identities(members).
Each of the team members sign off, attest, to the code that is actually running on the Pis, thinking docker would be super handy for this.  
This would allow like a trust circle jerk.
The proof would be in the jerky, if the sensors were lying it's easy enough to fuck up jerky that people would probably get sick from it.
One sensor could be a camera so you could film whoever is doing the food prep, people can see them wash their hands and all the stuff not easily captured by a temperature and humidity sensors, you could also use it to film analog gauges to reduce the trusted surface area.

### Tokens
With all this data why not tokenize each piece tying it to it's production data.
It wouldn't be much more than a fancy receipt but it could be traded as a collectable, who knows?
It could be used for future discounts the possibilities are endless.
One of those could be to make a prediction market of sorts.
Where you could issue tokens and then attach production data to them.
The earlier you buy the token the more risky the investment, as it nears completion risk diminishes.
Trust is essentially a lack of risk, predictability.
If there are identities and reputations tied to past production runs and those same identities are issuing tokens for another production run that doesn't yet exist there is much less of a chance of them not coming through with it.


### Exit Scams and Tyrannical Systems
The only way to prevent exit scams in reputation is to create tyrannical system which doesn't allow for exit.
Due to the nature of blockchains data persists indefinitely and censorship is not possible.
This means ruining your reputation doesn't allow for a do over.
However, humans are much the same at least at the small scale, you ruin your reputation people don't forget it.
We do forget though, selectively, its up to the individuals with knowledge of past behavior to not re share or continue to hold it against others.
The system makes no judgement calls, somethings are too dangerous to be forgotten, having a system that doesn't is a more capable system than those that do.
A system in individual's control can collectively chose to forget, but a system which forgets automatically takes that choice away from individuals.
