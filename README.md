Fine grain tracking can allow

Trust and verify as a way around regulation.
The general jist of it is, use the propsal system as a process tracking system that allows you to attach production data to individual pieces of jerky.
The goal is to use their proposal system's code base for an alternative use.
This repo is a Proof of Concept designed as an entry into the [Decred](https://decred.org) [Politeia Challenge.](/PoliteiaChallenge.md)

## Proof of Jerky Provence
