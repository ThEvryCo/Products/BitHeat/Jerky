### The World is a Database
A peculiar one but one none the less.
To write you simply need to apply energy to change the state of the database.
Values and configuration of data you write to the physical world make difference in their cost.
These costs are described by the laws of physics(thermodynamics).
With our current computational capabilities, mostly bandwidth actually, we are limited in the amount of data that we can effectively process on a synchronous basis(blockchains).
For the energy expended physically moving around private keys is actually competitive to on chain costs.
Physical tokens have been used since the invention of money.
The physicality makes it easy for people to conceptualize about the security implications of secret information(private keys).
It's also relatable as that is always what money has been, physical.

### Undersea Treasure
If you've been in the bitcoin space you've probably heard of the (stone money islanders)[https://www.npr.org/sections/money/2011/02/15/131934618/the-island-of-stone-money].
This is what has happened with bitcoin.
Except instead of a big as rock at the bottom of the ocean, we have bitcoin stuck behind a clogged pipe, equally unreachable.
What did the islanders do?
They just kept track of who owned it.
Instead of trying to move the bitcoin, just pass the reference to it, in this case a coin in the physical world.
Wait isn't bitcoin supposed to solve the problem of who has what?
Yes but the digital database is to expensive to write to so we'll offload it to physical pieces.

What the islanders had to keep this physical accounting straight was a small population and a common history, as to say it was people said it was.
Depending who was claiming what determined it's validity, there was group consensus on the state of database via human interaction.
This worked well because the stone money was only for large or meaningful transactions so it was relatively easy to keep everyone in consensus.
This consensus worked loosely via a proof of authority, people in the community had varying, reputation based, input into consensus since they were only valuable within the community.

How do we scale this beyond a common shared human hierarchy?
What I'm proposing is vaguely similar tweaked to take advantage of the more complex primitives computers and Public key cryptography.
The authority would be simply signing the firmware and tying each instance/serial number to an identity, in this case a brand.
Essentially Intel's SGX without having to trust Intel, instead trusting a brand whose only value is to be trusted in providing secure devices.
PoA has the advantage of being very compact to in terms of computational and storage requirements.
You only need to store the list of firmwares signed by the brands you trust.
With one time programmable microcontroller once programmed you can ensure that if the originator is trustworthy save for a bug you can count on the output/behavior.

The problem with digital gold is that it is digital, there aren't any physical ways to directly tell us what it's worth of anything at all.
The second part of this PoA is a verifier with physical tokens this would be the thing that stores the list of coins minted by brands you trust.
These could be produced by anyone potentially, ideally many vendors would produce them.
Verifiers could be completely offline, potentially loading up signatures on an sd card to allow for even cheaper construction.
These could be low power devices if they have the required public signature hardware accelerators.
The verifiers would only need to be updated with new signatures if it is trying to verify a coin minted after the last update.
Another scenario where having an up to date database is useful is when there are vulnerability updates if a product line of coins has a security flaw, alerting you to be suspect of coins of that design or version of firmware.
Even if they are compromised the coins on them will still be secure due to their offline nature.
However it might be hard to physically trade them since they can no longer trust their security guarantees.

The verifier itself would only consist of storage for minting attestations, power source, public key accelerators, and human I/O(ie. screen and buttons).
When accepting coins you could put a stack on the verifier, it sends a challenge(nounce) to all of the coins, which respond with their replies signing their balance along with the nounce sent from the verifier.
If the coin is on the list and the signature checks out you can trust the balance it is reporting.
