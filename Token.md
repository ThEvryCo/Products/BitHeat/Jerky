### Proof of Jerky tokenization

This is a proof of concept for entry into the Politeia Decred Challenge.

The goal is to make a proof jerky token based on provenance of production.

### Trusted Data Bootstrap
To trust the sensor data people will need to trust someone to set them up.

***rough workflow***
* set up politeia server
* register an identity
* use that identity to attest to other identities(sensors)
* The first sensor will be a raspberry pi camera
* This camera will always be on logging
* With the camera rolling the sensors and their identities will be added
* sensors will continuously record to their own data structures(git if feasible)
* when proving the existence of jerky the BitHeat identity will cherry pick the data relevant to that piece of Jerky

The goal of all this is to generate enough data to make it hard to fake the creation of a piece of jerky.
