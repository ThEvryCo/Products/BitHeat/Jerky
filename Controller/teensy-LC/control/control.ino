#include <PID_v1.h>
#include <Adafruit_BME280.h>



//control loop vars
double blowerPWM = 0;
float jerkHum1 = 0;
float jerkHum2 = 0;
double jerkTemp1 = 0;
float jerkPres1 = 0;
float jerkTemp2 = 0;
double inTemp = 0;
float waterHotTemp = 0;
double coolingPWM = 0;
// display vars
float blower = 0;
float cooling = 0;

// Pin definitions
const int waterTRPin = A0;
const int jerkSensCS0 = 10;
//const int jerkSensCS1 = ?;
const int blowerPWMPin = 17;
const int fanFakerPin0 =3;
const int fanFakerPin1 =4;
const int coolingPWMPin = 20;
const int led = 13;

Adafruit_BME280 bme(jerkSensCS0);
//Adafruit_BME280 bme1(jerkSensCS1);
double maxTemp = 88.250;
double dhTempPoint = 65;

PID maxTempPID(&inTemp, &coolingPWM, &maxTemp, 1, 0.05, 0, REVERSE);
PID dhTemp(&jerkTemp1,&blowerPWM,&dhTempPoint,1,0.05,0,DIRECT);

String tempString = "";
boolean msgComplete = false;


void setup() {
  // serial vars
  Serial.begin(9600);
  String tempString = "";
  boolean msgComplete = false;
  tempString.reserve(10);

  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);
  pinMode(waterTRPin,INPUT);
  pinMode(jerkSensCS0,OUTPUT);
 // pinMode(jerkSensCS1,OUTPUT);
  pinMode(blowerPWMPin ,OUTPUT);
  pinMode(fanFakerPin0 ,OUTPUT);
  analogWrite(fanFakerPin0, 127);
  pinMode(fanFakerPin1 ,OUTPUT);
  analogWrite(fanFakerPin1, 127);
  analogWriteFrequency(20,23437.5);
  pinMode(coolingPWMPin,OUTPUT);

  maxTempPID.SetMode(AUTOMATIC);
  dhTemp.SetMode(AUTOMATIC);

  if (!bme.begin()) {  
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
  }

}

void loop() {
  if(msgComplete){
    //char whString[25];
    //dtostrf(waterHotTemp,6,3,whString);
    //char jh1String[25];
    //dtostrf(jerkHum1,6,3,jh1String);
    //char jh2String[25];
    //dtostrf(jerkHum2,6,3,jh2String);
    //char jt1String[25];
    //dtostrf(jerkTemp1,6,3,jt1String);
    //char jt2String[25];
    //dtostrf(jerkTemp2,6,3,jt2String);
    //char coolingString[25];
    //dtostrf(coolingPWM,6,3,coolingString);
    //char blowerString[25];
    //dtostrf(blowerPWM,6,3,blowerString);
    String jt1String = "Jerky Temp 1: ";
    jt1String +=jerkTemp1;
    String jh1String = "Jerk Humudity 1: ";
    jh1String += jerkHum1;
    String jp1String = "Jerk Pressure 1: ";
    jp1String += jerkPres1;
    String blowerString = "Blower Power: ";
    blowerString += blowerPWM;
    String coolingString = "Cooling Power: ";
    coolingString += coolingPWM;
    Serial.println(jt1String);
    Serial.println(jh1String);
    Serial.println(jp1String);
    Serial.println(blowerString);
    Serial.println(coolingString);
    Serial.println("----------------------");
   /* String sprint = "";
    sprint += whString;
    sprint += ',';
    sprint += jh1String;
    sprint += ',';
    sprint += jt1String;
    sprint += ',';
    sprint += jh2String;
    sprint += ',';
    sprint += jt2String;
    sprint += ',';
    sprint += cooling;
    sprint += ',';
    sprint += blower;
   // Serial.println(sprint);
   */
    msgComplete = false;
    inTemp = tempString.toFloat();
    maxTempPID.Compute();
    analogWrite(coolingPWMPin,coolingPWM);
  }
  jerkTemp1 = bme.readTemperature();
  jerkHum1 = bme.readHumidity();
  jerkPres1 = bme.readPressure();
  dhTemp.Compute();
  analogWrite(blowerPWMPin, 200);
}
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    if(inChar != '\n'){
      tempString += inChar;
    }
    else{
      msgComplete = true;
    }
  }
}
