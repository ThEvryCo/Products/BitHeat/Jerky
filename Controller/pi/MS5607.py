

from spidev import SpiDev
import RPi.GPIO as gpio
import time

spi = SpiDev()

spi.open(0,0)

#spi.spi_bus(0)
#spi.spi_device(0)
spi.max_speed_hz = 8000000
spi.mode=0
spi.bits_per_word=8
spi.lsbfirst=False

def reset():
	return	spi.xfer([0b00011110],spi.max_speed_hz,2800,spi.bits_per_word)
def getCal():
	promAdd = 0b10100010
	r = []
	while promAdd <= 0xAC:
		c = spi.xfer2([promAdd,0,0])
		v = c[1]<<8|c[2]
		r.append(v)	
		promAdd = promAdd +2
	return r

def conPressure():
	add = 0b01001000
	spi.xfer([add])
	time.sleep(0.009)




def conTemp():
	add = 0b01011000
	spi.xfer([add])
	time.sleep(0.009)


	
def readADC():
	add = 0
	c = spi.xfer2([add,0,0,0])
	v = c[1]<<8
	v = v | c[2]
	v = v <<8
	v = v | c[3]
#	v = c[1]<<16|c[2]<<8|c[3]
	return v

def readTemp():
	conTemp()
	return readADC()

def getTemp(cal):
	t = (2000 + getDT(cal)*cal[5]/2**23)
	t = (float(t))/100
	return t


def getDT(cal):
	d2 = readTemp()
	return d2 - cal[4]*2**8

def getPressure(cal):
	dt  = getDT(cal)
	off = cal[1]*2**17+(cal[3]*dt)/2**6
	sens =  cal[0]*2**16+(cal[2]*dt)/2**7
	conPressure()
	d1 = readADC()
	p = (d1*sens/2**21-off)/2**15
	return float(p)/100
	
cal = getCal()
print(cal)
while 1:
	print 'Temp      = {0:0.3f} deg C'.format(getTemp(cal))
	print 'Pressure  = {0:0.2f} hPa'.format(getPressure(cal))
	print '------------------------------'
	time.sleep(.2)
spi.close()
