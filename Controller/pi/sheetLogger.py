
from __future__ import print_function
import httplib2
import os

import MS5607 as ms
import pprint
import RPi.GPIO as GPIO
import time

import ConfigParser
import threading
import logging


from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/drive'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'BitHeat'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def main():
    """Shows basic usage of the Sheets API.

    Creates a Sheets API service object and prints the names and majors of
    students in a sample spreadsheet:
    https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

    spreadsheetId = '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms'
    rangeName = 'Class Data!A2:E'
    result = service.spreadsheets().values().get(
        spreadsheetId=spreadsheetId, range=rangeName).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        print('Name, Major:')
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            print('%s, %s' % (row[0], row[4]))

credentials = get_credentials()
http = credentials.authorize(httplib2.Http())
discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                'version=v4')
sService = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discoveryUrl)
dService = discovery.build('drive','v3',http=http)

start = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime())
config = ConfigParser.ConfigParser()
config.read("config.ini")
#resume = raw_input('Resume last sheet y/n: ')
resume = n
if resume == 'y':
    sheetId =  config.get("History","lastSheet")
    print("Using Sheet with sheetId: "+sheetId)
    val = []
else:
    print("Creating google sheet "+start)
    #Creates spreadsheet with time of start as name and moves it into the cycler.2/data folder
    request = sService.spreadsheets().create(body={"spreadsheetId":"","properties":{"title":start}})
    response = request.execute()
    sheetId = response['spreadsheetId']
    print("Sheet created with ID: "+sheetId)


    print("Moving Sheet to Heater.0/data folder")
    request = dService.files().list(q="name='Heater.0'")
    response = request.execute()
    parentFolder = response['files'][0]['id']
    q = "name='data' and '"+ parentFolder +"' in parents"
    request = dService.files().list(q=q)
    response = request.execute()
    dataFolder = response['files'][0]['id']
    parent = dService.files().get(fileId=sheetId,fields='parents').execute()
    dService.files().update(fileId=sheetId,addParents=dataFolder,removeParents=parent['parents'][0],fields='id,parents').execute()
    print("Sheet moved.")
    cfgfile = open("config.ini","w")
    config.set("History","lastSheet",sheetId)
    config.write(cfgfile)
    cfgfile.close()
    val = [['date time','temp C', 'pressure mBar', 'cycle #','in cycle']]



# get calibration from temp/pressure sensor
print("getting calibration data")
cal = ms.getCal()



def sheetAppend(sheetId,body):
    sService.spreadsheets().values().append(spreadsheetId=sheetId, range='Sheet1!A1:E1',valueInputOption='USER_ENTERED', body=body).execute()

cycle = 0
cycleCount = int(config.get("History","globalCycleCount"))
seconds = 0
readings = 30
on = 1
off = 0
print('Starting control loop')
while 1:
    temp = ms.getTemp(cal)
    pres = ms.getPressure(cal)
    while temp > 130:
	temp = ms.getTemp(cal)
    while pres > 3000:
        pres = ms.getPressure(cal) 
    liveVal = [time.strftime('%Y/%m/%d %H:%M:%S',time.localtime()),temp,pres,cycleCount,cycle]
 
    val.append(liveVal)
    readings += 1
    body = {
                "majorDimension":"ROWS",
                "range":"Sheet1!A1:E1",
                "values": val
            }
    print(liveVal)
    print(threading.activeCount())
    if threading.activeCount() < 3 and readings > 29:
        t = threading.Thread(target=sheetAppend,args=(sheetId,body))
        t.start()
	val = []
	readings = 0

    if cycle:
        if temp < 50:
            cycle = 0
            GPIO.output(pumpPin, off)
    else:
        if temp > 85:
            cycle = 1
            cycleCount += 1
	    cfgfile = open("config.ini",'w')
	    config.set("History","globalCycleCount",cycleCount)
	    config.write(cfgfile)
            cfgfile.close()
            GPIO.output(pumpPin, on)
    time.sleep(1)
    t.join(0)

